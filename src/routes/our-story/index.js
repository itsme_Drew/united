import React from 'react'
import DefaultLayout from '../../layouts/default'
import FeatureStory from '../../components/features/story'
import Image from '../../components/image'
import StaffStacey from '../../components/staff/stacey'
import StaffDemaio from '../../components/staff/demaio'
import StoryNewBuilding from '../../images/story-new-building.jpg'
import Contact from '../../components/contact'

class OurStory extends React.Component {
  render() {
    return (
      <DefaultLayout pagetitle={this.props.pagetitle}>
        <div className="story-page">
          <div className="container container--grid">
            <div className="row">
              <div className="col col--md-8 col--md-push-2">
                <FeatureStory title="our new and improved office" color="blue">
                  <Image imgSrc={StoryNewBuilding} alt="renovated chiropractic office in sarasota" />
                  <br/><br/>
                  <h3>Come and enjoy our new spa like atmosphere.</h3>
                  <p>Our modern and up to date office offers new high-tech treatment options.</p>
                </FeatureStory>
              </div>
            </div>
            <FeatureStory title="our staff" color="green">
              <StaffDemaio />
              <StaffStacey />
            </FeatureStory>
          </div>
          <Contact />
        </div>
      </DefaultLayout>
    );
  }
}

export default OurStory;
