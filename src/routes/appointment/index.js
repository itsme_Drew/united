import React from 'react'
import { Link } from 'react-router-dom'
import DefaultLayout from '../../layouts/default'
import ContactForm from '../../components/forms/contact'
import OfficePaperworkPdf from '../../images/office-paperwork.pdf'

class Appointment extends React.Component {
  render() {
    return (
      <DefaultLayout pagetitle={this.props.pagetitle}>
        <div className="appointment-page container container--grid">
          <div className="row">
            <div className="col col--md-5">
              <ContactForm title="Your path to comfort starts now" />
            </div>
            <div className="col col--md-5 col--md-push-1">
              <h3>Contact Us</h3>
              <p>Phone: <a href="tel:+19419224222">(941) 922-4222</a></p>
              <p>Address: <a href="https://www.google.com/maps/dir//27.251126,-82.498036/@27.251126,-82.498036,16z?hl=en-US" target="_blank" rel="noopener noreferrer">7242 S Beneva Rd, Sarasota, FL 34238</a></p>
              <h3>Office Hours</h3>
              <ul>
                <li>Monday: 9am-5pm</li>
                <li>Tuesday: 9am-12pm</li>
                <li>Wednesday: 9am-5pm</li>
                <li>Thursday: 2:30-6:30pm</li>
                <li>Friday: 9am-5pm</li>
                <li>Saturday: 10am-12pm</li>
                <li>Sunday: Closed</li>
              </ul>
              <p className="disclaimer">*If you have a chiropractic emergency, we will see you outside of our normal business hours.</p>
              <h3>Paperwork</h3>
              <p>Save time at the office by printing and completing your paperwork before coming into your next appointment.</p>
              <Link to={OfficePaperworkPdf} target="_blank">Print Paperwork</Link>
            </div>
          </div>
        </div>
      </DefaultLayout>
    );
  }
}

export default Appointment;
