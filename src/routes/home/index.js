import React from 'react'
import PageLayout from '../../layouts/page'
import ButtonPrimary from '../../components/buttons/primary'
import Rating from '../../components/rating'
import GetWellImg from '../../images/get-well.jpg'
import BeWellImg from '../../images/be-well.jpg'
import StayWellImg from '../../images/stay-well.jpg'
import StaffDemaio from '../../components/staff/demaio'
import Contact from '../../components/contact'
import Testimonial from '../../components/testimonial'
import { TweenMax, Back } from 'TweenMax'
import ScrollMagic from 'scrollmagic'
import 'animation.gsap'
import 'debug.addIndicators'

class Home extends React.Component {
  render() {
    return (
      <PageLayout>
        <div className="home-page">
          <section className="hero-main">
            <div className="container container--grid">
              <div className="row">
                <div className="hero-main__content col">
                  <div className="headline">
                    <h1 className="headline__heading">Your pain is <strong>unique</strong>. <span>Your <strong>care</strong> should be too.</span></h1>
                  </div>
                  <div className="row">
                    <div className="hero-main__details col col--md-8 col--md-push-2">
                      <p className="hero-main__message">Leading Sarasota County in Chiropractic care. Are you in pain? We can help! Take the first step today.</p>
                      <ButtonPrimary className="hero-home__button" linkTo="/appointment">schedule appointment</ButtonPrimary>
                      <div className="hero-main__rating" id="ani__1-1">
                        <Rating count="5" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="testimonials">
            <div className="container container--grid">
              <div className="row">
                <div className="col col--md-8 col--md-push-2">
                  <Testimonial />
                </div>
              </div>
            </div>
          </section>
          <section className="demaio">
            <div className="container container--grid">
              <StaffDemaio />
            </div>
          </section>
          <section className="plan">
            <div className="container container--grid">
              <div className="row">
                <div className="col">
                  <div className="plan__header" id="banjo">
                    <h2 className="plan__heading">Our plan to a new healthy you</h2>
                  </div>
                </div>
              </div>
              <div className="step--1 row">
                <div className="step-info col col--md-5">
                  <div className="step-info__header">
                    <span className="step-info__number">01</span>
                    <h4 className="step-info__title">get well</h4>
                  </div>
                  <p className="step-info__description">
                    Relief care focuses on <b>JUST</b> the symptoms. In this case it is back pain. Relief care would be similar to taking aspirin. Although it temporarily alleviate the back pain, it does nothing to correct the cause.
                  </p>
                </div>
                <div className="col col--md-7">
                  <img className="step__image" src={GetWellImg} alt="chiropractic plan: get well"/>
                </div>
              </div>
              <div className="step--2 row">
                <div className="col col--md-7">
                  <img className="step__image" src={BeWellImg} alt="chiropractic plan: be well"/>
                </div>
                <div className="step-info col col--md-5">
                  <div className="step-info__header">
                    <span className="step-info__number">02</span>
                    <h4 className="step-info__title">be well</h4>
                  </div>
                  <p className="step-info__description">
                    The goal of corrective care is the removal or reduction of the cause of your problem, allowing the relief or removal of the symptoms. Corrective care is necessary not only to relieve or reduce a patient’s pain or symptoms, but to also remove the actual cause of the problem.
                  </p>
                </div>
              </div>
              <div className="step--2 row mobile">
                <div className="step-info col col--md-5">
                  <div className="step-info__header">
                    <span className="step-info__number">02</span>
                    <h4 className="step-info__title">be well</h4>
                  </div>
                  <p className="step-info__description">
                    The goal of corrective care is the removal or reduction of the cause of your problem, allowing the relief or removal of the symptoms. Corrective care is necessary not only to relieve or reduce a patient’s pain or symptoms, but to also remove the actual cause of the problem.
                  </p>
                </div>
                <div className="col col--md-7">
                  <img className="step__image" src={BeWellImg} alt="chiropractic plan: be well"/>
                </div>
              </div>
              <div className="step--3 row">
                <div className="step-info col col--md-5">
                  <div className="step-info__header">
                    <span className="step-info__number">03</span>
                    <h4 className="step-info__title">stay well</h4>
                  </div>
                  <p className="step-info__description">
                    Taking correct care of our bodies is very important in our everyday lives. Without continued chiropractic care, your muscles and vertebrae will return to their unhealthy positions. Part of the stay well step retrains and strengthens muscles to assume a more correct position. This is optimal care at it's finest.
                  </p>
                </div>
                <div className="col col--md-7">
                  <img className="step__image" src={StayWellImg} alt="chiropractic plan: stay well"/>
                </div>
              </div>
            </div>
          </section>
          <Contact />
        </div>
      </PageLayout>
    );
  }

  componentDidMount() {
    const controller = new ScrollMagic.Controller();

    //Create Tweens
    const tween__1_1 = TweenMax.staggerFromTo("#ani__1-1 .stars__star", 4, {left: 700}, {left: 0, opacity: 1, ease: Back.easeOut}, 0.15);

    //Create Scenes
    new ScrollMagic.Scene({triggerElement: "#ani__1-1", duration: 200})
					.setTween(tween__1_1)
					.addTo(controller);
  }

}

export default Home;
