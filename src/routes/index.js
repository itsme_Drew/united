import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './home';
import OurStory from './our-story';
import Services from './services';
import WhatToExpect from './your-first-visit';
import Appointment from './appointment';
import Privacy from './privacy';
import Service from './services/service';

class Main extends React.Component {
  render() {
    return (
      <main>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path="/our-story" render={()=><OurStory pagetitle='our story' />}/>
          <Route exact path="/services" render={()=><Services pagetitle='services' />}/>
          <Route path="/services/:sectionName" render={Service}/>
          <Route path="/your-first-visit" render={()=><WhatToExpect pagetitle='your first visit' />}/>
          <Route path="/appointment" render={()=><Appointment pagetitle='appointments' />}/>
          <Route path="/privacy" render={()=><Privacy pagetitle='privacy policy' />}/>
        </Switch>
      </main>
    );
  }
}

export default Main;
