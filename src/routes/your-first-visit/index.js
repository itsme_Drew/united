import React from 'react'
import DefaultLayout from '../../layouts/default'
import FeatureStory from '../../components/features/story'
import Image from '../../components/image'
import Contact from '../../components/contact'
import HeaderImage from '../../images/stacy-and-patient-1.jpg'
import ButtonPrimary from '../../components/buttons/primary'
import OfficePaperworkPdf from '../../images/office-paperwork.pdf'

class YourFirstVisit extends React.Component {
  render() {
    return (
      <DefaultLayout pagetitle={this.props.pagetitle}>
        <div className="what-to-expect-page">
          <div className="container container--grid">
            <FeatureStory color="blue">
              <Image imgSrc={HeaderImage} alt="your first visit"/>
              <p>We respect and value your time. Please allow an hour for your first visit which includes:</p>
              <ul className="feature-story__list">
                <li className="feature-story__list-item">Consultation with the Doctor</li>
                <li className="feature-story__list-item">Initial Examination</li>
                <li className="feature-story__list-item">Doctor's review of Diagnosis</li>
                <li className="feature-story__list-item">Physiotherapy</li>
                <li className="feature-story__list-item">Chiropractic Adjustment</li>
                <li className="feature-story__list-item">Treatment plan review</li>
              </ul>
              <p>Save time at the office by printing and completing your paperwork before coming into your next appointment.</p>
              <ButtonPrimary linkTo={OfficePaperworkPdf} linkTarget="_blank">New Patient Paperwork</ButtonPrimary>
            </FeatureStory>
          </div>
          <Contact title="schedule your first appointment" />
        </div>
      </DefaultLayout>
    );
  }
}

export default YourFirstVisit;
