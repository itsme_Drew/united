import React from 'react'
import { Link } from 'react-router-dom'
import DefaultLayout from '../../layouts/default'
import FeatureStory from '../../components/features/story'
import Plan from '../../components/plan'
import Testimonial from '../../components/testimonial'
import Image from '../../components/image'
import Contact from '../../components/contact'
import AdjustmentImage from '../../images/adjustment.jpg'
import PhysiotherapyImage from '../../images/physiotherapy.jpg'
import ColdLaserImg from '../../images/cold-laser.jpg'
import StretchingImage from '../../images/stretching-on-mats-PGKJN6U.jpg'

const Services = (props) => {
  return (
    <DefaultLayout pagetitle={ props.pagetitle }>
      <div className="services-page">
        <section className="container container--grid">
          <div className="row">
            <div className="col col--md-8 col--md-push-2">
              <FeatureStory title="our simple plan to leave you feeling great" color="purple">
                <Plan />
              </FeatureStory>
            </div>
          </div>
        </section>
        <section className="testimonials">
          <div className="container container--grid">
            <div className="row">
              <div className="col col--md-8 col--md-push-2">
                <Testimonial />
              </div>
            </div>
          </div>
        </section>
        <section className="service blue">
          <div className="container container--grid">
            <div className="row">
              <div className="col col--md-5"><Image imgSrc={AdjustmentImage} alt="chiropractic adjustments"/></div>
              <div className="col col--md-7">
                <h2 className="service__heading">adjustments</h2>
                <p>There are over one-hundred chiropractic adjustment techniques. We practice thirteen of the most widely performed and researched techniques to restore proper movement to a joint that's not moving properly. The action of the technique may be a quick thrust or a slow pressure. This can be done by hand or by using a specialized chiropractic instrument. The technique that the doctor chooses for you will depend on your specific needs.</p>
              </div>
            </div>
          </div>
        </section>
        <section className="service green">
          <div className="container container--grid">
            <div className="row">
              <div className="col col--md-8">
                <h2 className="service__heading">cold laser therapy</h2>
                <p>Low level laser therapy is a non-invasive, fast and effective modality that has been proven in clinical trials to reduce pain, reduce edema, and promote healing. The potential applications of low-level laser (3LT®) are almost limitless, however; to date Erchonia has received market clearance for Neck and Shoulder Pain, Breast Augmentation, Acne, Laser Assisted Liposuction, and Non-Invasive Body Contouring.  Erchonia continues to conduct clinical trials on other applications.</p>
              </div>
              <div className="col col--md-4"><Image imgSrc={ColdLaserImg} alt="low level cold laser therapy"/></div>
            </div>
          </div>
        </section>
        <section className="service purple">
          <div className="container container--grid">
            <div className="row">
              <div className="col col--md-5"><Image imgSrc={PhysiotherapyImage} alt="physiotherapy" /></div>
              <div className="col col--md-7">
                <h2 className="service__heading">physiotherapy</h2>
                <p>Rather than using drugs or surgery, Dr. DeMaio and staff treats muscle injuries using myofascial release, moist heat treatment, ice treatments, sports taping, and exercise techniques. The goal of physiotherapy is to reduce muscle tension and pain.</p>
              </div>
            </div>
          </div>
        </section>

        <section className="service white">
          <div className="container container--grid">
            <div className="row">
              <div className="col">
                <div className="col col--md-5"><Image imgSrc={StretchingImage} alt="chiropractic adjustments"/></div>
                <div className="col col--md-7">
                  <h2 className="service__heading">chiropractic care designed for you</h2>
                  <p>We treat each of our patients as individuals. Our goal is to create a plan of action that is specific for you and help you get back to doing what you love, pain free. We successfully treat many conditions including</p>
                  <ul className="service__list">
                     <li><Link to="/services/headaches">headaches</Link></li>
                     <li><Link to="/services/hip-sacroiliac-leg">hip, sacroiliac & leg problems</Link></li>
                     <li><Link to="/services/shoulder-arm-hand">shoulder arm hand</Link></li>
                     <li><Link to="/services/low-back-pain">low back pain</Link></li>
                     <li><Link to="/services/scoliosis">scoliosis</Link></li>
                     <li><Link to="/services/stress">stress</Link></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Contact />
      </div>

    </DefaultLayout>
  );
}

export default Services;
