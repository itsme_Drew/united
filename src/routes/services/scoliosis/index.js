import React from 'react'
import ScoliosisImg1 from '../../../images/scoliosis-1.png'
import ScoliosisImg2 from '../../../images/scoliosis-2.png'

const scoliosis = ({match}) => {
  return (
    <article>
      <h2 className="service__heading">scoliosis</h2>
      <section className="two-col">
        <img src={ScoliosisImg1} alt="scoliosis"/>
        <p>
          Everyone's spine has natural curves.  These curves round our shoulders and make our lower back curve slightly inward.  But some people have spines that also curve from side to side.  Unlike poor posture, these curves can't be corrected simply by learning to stand up straight.
        </p>
      </section>
      <section className="two-col">
        <img src={ScoliosisImg2} alt="scoliosis c and s shape"/>
        <p>
          On an x-ray, the spine of a person with scoliosis looks more like an "S" or a "C" than a straight line.  Some of the bones in a scoliotic spine also may have rotated slightly, making the person's waist or shoulders appear uneven.
        </p>
      </section>
      <section>
        <h3>“Warning Signs” for Scoliosis</h3>
        <p>
          There are several different "warning signs" to look for to determine if someone has Scoliosis.
          <ul>
            <li>One shoulder may appear higher than the other</li>
            <li>One hip may appear higher than the other</li>
            <li>Head is not centered directly above the pelvis</li>
            <li>One shoulder blade may stick out more than the other</li>
            <li>The ribs are higher on one side when bending forward</li>
            <li>The waistline may be flat or uneven on one side</li>
          </ul>
        </p>
      </section>
      <section>
        <h3>Common Symptoms</h3>
        <ul>
          <li>Low back pain</li>
          <li>Muscle ache or spasm</li>
          <li>Numbness and tingling down an arm</li>
          <li>Shortness of breath</li>
          <li>Chest pains</li>
          <li>General discomfort around the shoulder blade area</li>
        </ul>
      </section>
      <section>
        <p><strong>Therapeutic treatment options are available to help relieve the physical side effects of Scoliosis and may even help stop the progression of Scoliosis.</strong></p>
      </section>
    </article>
  );
}

export default scoliosis;
