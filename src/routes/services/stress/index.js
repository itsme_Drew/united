import React from 'react'

const scoliosis = ({match}) => {
  return (
    <article>
      <section>
        <h2>what causes stress?</h2>
        <p>Things that cause stress are called “stresses” They are all around us. One might ask … “What happens when you face stress?”  First our body quickly prepares itself for confrontation. A message will travel from your brain to your endocrine system in a “fight or flight” response. IF the stress ends, your body will resume back to normal. Second, IF the stress continues, you go into the stage of resistance. At this point your body will try to adapt to the stress. You will run from or fight the threat; you tense your muscles; raise your fever; you struggle to prevail. Finally, if the stress STILL continues, you may pass into the stage of exhaustion. Your body can not be primed for action 24/7. Your system will fall, ill-health will begin and chronic dis-cease.</p>
      </section>
      <section>
        <h2>chronic stress</h2>
        <p>Living in society of not fully fight but yet not fully flight, causes you to carry around unresolved stress which is “chronic stress” most of the time.  When dealing with chronic stress, one can never relax. Your muscles will always be tensed, energy always will be drained in results your mind and body become fatigued. This will cause a person to be susceptible to colds, infection, ulcers, digestive troubles, high blood pressure, asthma, arthritis, heart disease and premature aging. This continued stress will slowly eliminates you of health and vitality, ultimately life itself.</p>
      </section>
      <section>
        <h2>mind-body</h2>
        <p>A person that is under extreme psychological stress is more than likely to have a weakened immune system. There are many techniques for stress reduction is meditation which many practitioners have claimed is able to affect a wide spectrum of ailments, particularly and infectious diseases.</p>
      </section>
      <section>
        <h2>the chiropractic approach</h2>
        <p>Chiropractic treatment relives your body from one of the highly destructive stresses you may experience. This is called “Vertebral subluxation”. This subluxation is a dangerous stressor that can damage your nerves, weaken your health and also exhaust your energy. This is a common painless condition that can be carried with you for years without knowing the damage it is causing. Doctors of chiropractic are the only professionals that specialized in correcting this “vertebral subluxation complex.</p>
      </section>
    </article>
  );
}

export default scoliosis;
