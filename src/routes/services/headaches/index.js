import React from 'react'

const Headaches = ({match}) => {
  return (
    <article>
      <section>
        <h2 className="service__heading">headaches</h2>
        <p>
          Headache “Triggers” can lead to subluxation. Chemical (ex. hormone changes) physical (ex. structural problems) an emotional (ex. anxiety) stressors can often cause headaches. Believe it or not, the commonly used headache medications can intensify the headaches!!

          All of the headache triggers can disrupt the delicate balance of your nervous system. Over a period of time, the headaches can alter the proper position of your spinal column, therefore, causing a series of “subluxations”. If this situation goes untreated, it will in return lead to irreversible degeneration.

          Chiropractic care can assist in correcting spinal misalignments created by numerous stresses.
        </p>
      </section>
      <section>
        <h3 className="service__heading">get rid of your pain</h3>
        <p>
          Chiropractic can help get rid of your headaches. There are endless amounts of different types of headaches. Below are the three major types. While identifying your specific type of headache, take a moment and think about what brings on your headache. Let’s start the road to recovery!
        </p>
        <h4>migraine headaches</h4>
        <ul>
          <li>Onesided pain associated with nausea</li>
          <li>Moderate to severe pain</li>
          <li>Throbbing sensation</li>
          <li>Extreme light, sound, and touch sensitivity</li>
          <li>Visual “aura”</li>
        </ul>
        <h4>tension headaches</h4>
        <ul>
          <li>Steady, dull pain that “caps” your scalp</li>
          <li>Mild to moderate pain</li>
          <li>Tension in your neck or head</li>
          <li>Mild light or sound sensitivity</li>
        </ul>
        <h4>cluster headaches</h4>
        <ul>
          <li>Pain near the forehead or around the eyes</li>
          <li>Excruciating pain</li>
          <li>Penetrating non-throbbing pain</li>
          <li>A series of headaches</li>
          <li>Watery, swollen eye</li>
        </ul>
      </section>
      <section>
        <h3 className="service__heading">let chiropractic help you</h3>
        <p>
          Headaches are a major symptoms of an issue located somewhere else in the body. The human body cervical spine is made up of a series of vertebral joints through which vital nerves pass. A chiropractic adjustment can relieve your pain by aligning any misaligned vertebra. This will correct the nerve interference that maybe causing your headaches. No general manipulation, no medication, no massage and no exercise can take the place of a chiropractic adjustment.
        </p>
      </section>
      <section>
        <h3 className="service__heading">a little known fact</h3>
        <p>
          A U.S study reported that 10 million Americans suffer from moderate to severe disability from various types of headaches!! About 90% of them could benefit from chiropractic care. Unfortunately, only 1 in 5 migraine sufferers seek treatment!! We all live busy lives, but taking care of your body should be your #1 priority. Get the help you need now, before it’s too late! It’s health care for today’s lifestyle!
        </p>
      </section>
    </article>
  );
}

export default Headaches;
