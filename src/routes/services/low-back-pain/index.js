import React from 'react'

const LowBackPain = ({match}) => {
  return (
    <article>
      <section>
        <p>If you have lower back pain, you are not alone. Nearly everyone at some point has back pain that interferes with work, routine daily activities, or recreation. Americans spend at least $50 billion each year on low back pain, the most common cause of job-related disability and a leading contributor to missed work. Back pain is the second most common neurological ailment in the United States — only headache is more common. Fortunately, most occurrences of low back pain go away within a few days. Others take much longer to resolve or lead to more serious conditions.</p>
        <p>Acute or short-term low back pain generally lasts from a few days to a few weeks. Most acute back pain is mechanical in nature — the result of trauma to the lower back or a disorder such as arthritis. Pain from trauma may be caused by a sports injury, work around the house or in the garden, or a sudden jolt such as a car accident or other stress on spinal bones and tissues. Symptoms may range from muscle ache to shooting or stabbing pain, limited flexibility and/or range of motion, or an inability to stand straight. Occasionally, pain felt in one part of the body may “radiate” from a disorder or injury elsewhere in the body. Some acute pain syndromes can become more serious if left untreated.</p>
        <p>Chronic back pain is measured by duration — pain that persists for more than 3 months is considered chronic. It is often progressive and the cause can be difficult to determine.</p>
        <p>Chiropractic care with Dr. Justine DeMaio is a great treatment option to combat both acute and chronic back pain. Dr. DeMaio uses a multidisciplinary approach which first helps eliminate or lessen the pain and second provides tools and treatment protocols which help prevent the back pain from returning.</p>
      </section>
      <section>
        <h2>Quick tips to a healthier back</h2>
        <p>During active chiropractic treatment for either acute or chronic back pain Dr. DeMaio offers customized exercise programs that will incorporate regular low-impact back exercises. These low-impact exercises will be appropriate for your age and designed to strengthen lower back and abdominal muscles. Once advised by Dr. DeMaio and during the corrective phase of treatment speed walking, swimming, or stationary bike riding 30 minutes a day can increase muscle strength and flexibility. Yoga can also help stretch and strengthen muscles and improve posture.</p>
        <p>Once examined by Dr. DeMaio these tips are helpful for a heathier back</p>
        <ul>
          <li>Always stretch before exercise or other strenuous physical activity.</li>
          <li>Don’t slouch when standing or sitting. When standing, keep your weight balanced on your feet. Your back supports weight most easily when curvature is reduced.</li>
          <li>At home or work, make sure your work surface is at a comfortable height for you.</li>
          <li>Sit in a chair with good lumbar support and proper position and height for the task. Keep your shoulders back. Switch sitting positions often and periodically walk around the office or gently stretch muscles to relieve tension. A pillow or rolled-up towel placed behind the small of your back can provide some lumbar support. If you must sit for a long period of time, rest your feet on a low stool or a stack of books.</li>
          <li>Wear comfortable, low-heeled shoes.</li>
          <li>Sleep on your side to reduce any curve in your spine. Always sleep on a firm surface.</li>
          <li>Ask for help when transferring an ill or injured family member from a reclining to a sitting position or when moving the patient from a chair to a bed.</li>
          <li>Don’t try to lift objects too heavy for you. Lift with your knees, pull in your stomach muscles, and keep your head down and in line with your straight back. Keep the object close to your body. Do not twist when lifting. </li>
          <li>Maintain proper nutrition and diet to reduce and prevent excessive weight, especially weight around the waistline that taxes lower back muscles. A diet with sufficient daily intake of calcium, phosphorus, and vitamin D helps to promote new bone growth. </li>
          <li>If you smoke, quit. Smoking reduces blood flow to the lower spine and causes the spinal discs to degenerate.</li>
        </ul>
      </section>
      <section>
        <h2>how the spine may produce back pain</h2>
        <p>Many different structures in the spine can cause back pain, potentially when:</p>
        <ul>
          <li>The large nerve roots that go to the legs and arms are irritated</li>
          <li>The smaller nerves that innervate the spine are irritated</li>
          <li>The large paired back muscles (erector spinae) are strained</li>
          <li>The bones, ligaments or joints themselves are injured</li>
          <li>The disc space itself is a source of pain.</li>
        </ul>
        <p>Therefore, a review of spinal anatomy is important to understand the causes of back pain, neck pain, and sciatica (LEG PAIN), and evaluate treatment options. Chiropractic care at DeMaio Chiropractic Wellness Center includes: Spinal manipulation is literally a "hands-on" approach in which professionally licensed specialists (doctors of chiropractic care) adjust spinal structures and restore spinal mobility. Once the back becomes stable and alignment is restored the next step is to start a series of exercises to strengthen the back and core muscles. Dr. Justine DeMaio initially focuses on decreasing the pain and or other   symptoms.</p>
      </section>
      <section>
        <h2>what structures make up the back?</h2>
        <p>Spinal anatomy is a remarkable combination of strong bones, flexible ligaments and tendons, large muscles and highly sensitive nerves. It is designed to be incredibly strong, protecting the highly sensitive nerve roots, yet highly flexible, providing for mobility on many different planes. Most of us take this juxtaposition of strength, structure and flexibility for granted in our everyday lives - until something goes wrong. Once we have back pain, we're driven to know what's wrong and what it will take to relieve the pain and prevent a recurrence.</p>
      </section>
    </article>
  );
}

export default LowBackPain;
