import React from 'react'
import { Route } from 'react-router-dom'
import DefaultLayout from '../../../layouts/default'
import OneCol from '../../../layouts/one-col'
import Headaches from '../headaches'
import Contact from '../../../components/contact'
import hipSacroiliacLeg from '../hip-sacroiliac-leg'
import lowBackPain from '../low-back-pain'
import ShoulderArmHand from '../shoulder-arm-hand'
import Stress from '../stress'
import Scoliosis from '../scoliosis'

const Service = ({match}) => {
  return (
    <DefaultLayout pagetitle={ match.params.sectionName }>
      <OneCol>
        <Route exact path='/services/headaches' render={Headaches}/>
        <Route exact path='/services/hip-sacroiliac-leg' render={hipSacroiliacLeg}/>
        <Route exact path='/services/low-back-pain' render={lowBackPain}/>
        <Route exact path='/services/shoulder-arm-hand' render={ShoulderArmHand}/>
        <Route exact path='/services/stress' render={Stress}/>
        <Route exact path='/services/scoliosis' render={Scoliosis}/>
      </OneCol>
      <Contact />
    </DefaultLayout>
  );
}

export default Service;
