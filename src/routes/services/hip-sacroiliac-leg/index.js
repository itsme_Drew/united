import React from 'react'

const Stress = ({match}) => {
  return (
    <article>
      <section>
        <h2>hip, sacroiliac & leg problems</h2>
        <p>
          Your hips are a masterpiece of engineering that chiropractic can keep balanced and in alignment. Studies have implicated, that an unbalanced hips, are the cause in most adults whom are suffering from low back pain. Dysfunction in the hip joint could be a primary cause in the development of lower back pain.
        </p>
      </section>
      <section>
        <h2>pelvic organs & your hips</h2>
          <p>The nerves beginning from the spinal column in the lower back as well as the hips travel to the kidneys, bladder, vagina, prostate and pelvic organs as well as the uterus and lower intestines. It is vital that these organs are clear of any irritation, compression and/or stress.</p>
          <p>When your hips are uneven, your legs then become uneven. In result, more pressure is placed on the longer leg when you walk: the knees, hips, ankle and feet on that side of your body may show pain and pressure.</p>
          <p>If you have a leg, knee, buttock, hip or low back weakness or pain? If you answered “yes”, it’s time to get a chiropractic checkup.</p>
      </section>
    </article>
  );
}

export default Stress;
