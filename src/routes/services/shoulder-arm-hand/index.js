import React from 'react'

const shoulderArmHand = ({match}) => {
  return (
    <article>
      <section>
        <p>Problems with the neck, shoulder and arms are often called different names by patients. Some people may blame their shoulder (or other joint) problems on “old age” even though their other shoulder, which doesn’t have any problem, is just as old.  Chiropractic success with shoulders, arm and hand problems are well documented. As a general rule, it is wise to explore drugless, non-surgical methods of healing first, before having to undergo more extreme paths of drugs and surgery.</p>
      </section>
    </article>
  );
}

export default shoulderArmHand;
