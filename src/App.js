import React, { Component } from 'react';
import Main from './routes';
import Header from './components/header';
import Footer from './components/footer';
import { Helmet } from 'react-helmet'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Helmet>
          <meta charSet="utf-8" />
          <title>Sarasota FL Chiropractor | United Joint & Spine Center</title>
          <meta name="Description" content="United Joint & Spine Center: Leading Sarasota County in Chiropractic care. Follow our simple plan to Get Well, Be Well, and Stay Well! Are you in pain? We can help! Take the first step today!" />
          <meta name="keywords" content="sarasota chiropractic,sarasota chiropractor,chiropractic,chiropractor,chiropractor near me, physiotherapy,cold laser therapy,chiropractic adjustments,chiropractic care,family chiropractic,back pain doctor,spinal decompression,chiro,chiropractic therapy,dr. justine demaio,wellness,health,joint,pain,sarasota chiropractors" />
        </Helmet>
        <Header />
        <Main />
        <Footer />
      </div>
    );
  }
}

export default App;
