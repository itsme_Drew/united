import React from 'react';

class PageLayout extends React.Component {
  render() {
    return (
      <div className="tpl-page">
       {this.props.children}
     </div>
    );
  }
}

export default PageLayout;
