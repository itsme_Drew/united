import React from 'react';

class OneCol extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="col">
          { this.props.children }
        </div>
      </div>
    );
  }
}

export default OneCol;
