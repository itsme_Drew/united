import React from 'react';
import PropTypes from 'prop-types'

class DefaultLayout extends React.Component {
  render() {
    return (
      <div className="tpl-default" >
        <div className="tpl-default__header">
          <h1 className="tpl-default__page-title">{this.props.pagetitle}</h1>
        </div>
        <div className="tpl-default__content">
          {this.props.children}
        </div>
     </div>
    );
  }
}

DefaultLayout.propTypes = {
  pagetitle: PropTypes.string.isRequired
}

export default DefaultLayout;
