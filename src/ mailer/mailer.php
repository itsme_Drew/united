<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require './PHPMailer/Exception.php';
require './PHPMailer/PHPMailer.php';
require './PHPMailer/SMTP.php';

$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);

$err = false;
$msg = '';
$email = '';
$domain = 'unitedchirocare.com';

//Apply some basic validation and filtering to the name
if (array_key_exists('name', $_POST)) {
    //Limit length and strip HTML tags
    $name = substr(strip_tags($_POST['name']), 0, 255);
} else {
    $name = '';
}

//Apply some basic validation and filtering to the phone
if (array_key_exists('comment', $_POST)) {
    //Limit length and strip HTML tags
    $comment = substr(strip_tags($_POST['comment']), 0, 255);
} else {
    $comment = '';
}

//Apply some basic validation and filtering to the phone
if (array_key_exists('phone', $_POST)) {
    //Limit length and strip HTML tags
    $phone = substr(strip_tags($_POST['phone']), 0, 255);
} else {
    $phone = '';
}

//Make sure the address they provided is valid before trying to use it
if (array_key_exists('email', $_POST) and PHPMailer::validateAddress($_POST['email'])) {
    $email = $_POST['email'];
} else {
    $msg .= "Error: invalid email address provided";
    $err = true;
}

if (!$err) {
    $mail = new PHPMailer;

    $mail->setFrom('webmaster@' . $domain, (empty($name) ? 'Contact form' : $name));
    $mail->addAddress('justine.demaio@gmail.com');
    $mail->addReplyTo($email, $name);
    $mail->Subject = 'New Contact Request';
    $mail->Body = "You have a new contact request\n\n" . "Name: " . $name . "\n\n" . "Email: " . $email . "\n\n" . "Phone: " . $phone . "\n\n" . "Comment: " . $comment;

    if (!$mail->send()) {
        $msg .= "Mailer Error: " . $mail->ErrorInfo;
    } else {
        $msg .= "Message sent!";
        echo "Message sent!";
    }
} else {
  echo "there was an error";
}


?>
