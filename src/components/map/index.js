import React from 'react'

class Map extends React.Component {
  render() {
    return (
      <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3546.9769460424336!2d-82.50074088494782!3d27.251252382982997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88c343df9a4dee85%3A0xd1280b1fba06be28!2sUnited+Joint+%26+Spine+Center+LLC!5e0!3m2!1sen!2sus!4v1508372536082"
            width="100%"
            height="450"
            allowFullScreen
            title="map"
      >
      </iframe>
    )
  }
}

export default Map;
