import React from 'react'
import ContactForm from '../forms/contact'
import PropTypes from 'prop-types'
import Map from '../map'
import Rating from '../rating'

class Contact extends React.Component {
  render() {
    return (
      <section className="contact">
        <div className="container container--grid">
          <div className="row">
            <div className="col col--md-5">
              <ContactForm title={this.props.title} />
            </div>
            <div className="col col--md-7">
              <Map />
              <div className="contact__ratings">
                <Rating count='0' />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

Contact.propTypes = {
  title: PropTypes.string
}

export default Contact;
