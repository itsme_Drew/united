import React from 'react'

class FormSuccess extends React.Component {
  render() {
    return (
      <div className="form-success">
        <h2 className="form-success__title">Thank you!</h2>
        <p className="form-success__message">A member of our team will contact you by the next business day to help you schedule your appointment.</p>
      </div>
    );
  }
}

export default FormSuccess;
