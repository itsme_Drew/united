import React from 'react'
import axios from 'axios'
import { Form, Text, Textarea } from 'react-form'
import classnames from 'classnames'
import PropTypes from 'prop-types'
import FormSuccess from '../success'

class ContactForm extends React.Component {
  constructor() {
    super();

    this.state = {
      formComplete: false,
      isSubmitted: false
    }

    this.renderForm = this.renderForm.bind(this)
    this.renderFormHeader = this.renderFormHeader.bind(this)
  }

  onFormSubmit = (values) => {
    let vm = this;

    const { name, phone, email, comment } = values;

    axios.post('https://www.unitedchirocare.com/mailer/mailer.php', {
          name,
          email,
          phone,
          comment
      })
      .then((result) => {
          vm.setState(prevState => ({
            isSubmitted: true
          }));
      });
  }

  renderFormHeader = () => {
    return (
      <div className="contact-form__header">
        <h2 className="contact-form__heading">
          {this.props.title ? this.props.title : 'when is a good time for you?'}
        </h2>
        <p className="contact-form__info">Our office will contact you to schedule your appointment to get started.</p>
      </div>
    )
  }

  renderForm = () => {
    return (
      <div className="contact-form__form">
        <Form
          onSubmit={this.onFormSubmit}
          validate={values => {
            const { name, email, phone, comment } = values
            return {
              name: !name ? 'A name is required' : undefined,
              email: !email ? 'A email is required' : undefined,
              phone: !phone ? 'A phone number is required' : undefined,
              comment: !comment ? 'Please let us know why you would like to be seen' : undefined,
            }
          }}
        >
          {({values, submitForm}) => {
            const isComplete = !!values.name && values.email && values.phone;

            return (
              <form onSubmit={submitForm}>
                <Text placeholder="name" field="name" />
                <Text placeholder="email" field="email" />
                <Text placeholder="phone" field="phone" />
                <Textarea placeholder="How can we help you?" field="comment" />
                <button className={classnames('contact-form__button', {isDisabled: !isComplete})} type="submit">Submit</button>
              </form>
            )
          }}
        </Form>
      </div>
    )
  }

  render() {
    return (
      <div className="contact-form">
        { this.state.isSubmitted ? null : this.renderFormHeader() }
        { this.state.isSubmitted ? <FormSuccess /> : this.renderForm() }
      </div>
    )
  }
}

ContactForm.propTypes = {
  title: PropTypes.string
}

export default ContactForm;
