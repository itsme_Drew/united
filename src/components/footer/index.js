import React from 'react'
import { Link } from 'react-router-dom'
import SiteNav from '../nav/site-menu/site-nav'

class Footer extends React.Component {
  render() {
    const currentYear = (new Date()).getFullYear()

    return (
      <div className="footer">
        <SiteNav />
        Copyright ©{currentYear} United Joint and Spine Center. All Rights Reserved. <Link className="footer__link" to="/privacy">Privacy Policy</Link>
      </div>
    )
  }
}

export default Footer;
