import React from 'react'

class featureHeader extends React.Component {
  render() {
    return (
      <div className="feature-story__header">
        <h2 className="feature-story__title">{this.props.title}</h2>
      </div>
    )
  }
}

export default featureHeader;
