import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import FeatureHeader from './header'

class FeatureStory extends React.Component {
  render() {
    let Color = this.props.color ? this.props.color : null;

    return (
      <div className={classnames('feature-story', Color)}>
        <FeatureHeader title={this.props.title} />
        {this.props.children}
      </div>
    )
  }
}

FeatureStory.propTypes = {
  title: PropTypes.string,
  color: PropTypes.string
}

export default FeatureStory;
