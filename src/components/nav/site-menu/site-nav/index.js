import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import classnames from 'classnames'

class SiteNav extends React.Component {
  render() {
    return (
      <nav
        className={classnames('site-nav__nav', this.props.className)}
        onClick={ this.props.onClick }
      >
        <NavLink className="site-nav__link" activeClassName='is-active' to="/our-story">Our Story</NavLink>
        <NavLink className="site-nav__link" activeClassName='is-active' to="/services">Services</NavLink>
        <NavLink className="site-nav__link" activeClassName='is-active' to="/your-first-visit">Your First Visit</NavLink>
        <NavLink className="site-nav__link" activeClassName='is-active' to="/appointment">Appointments</NavLink>
      </nav>
    );
  }
}

SiteNav.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string
}

export default SiteNav;
