import React from 'react'
import SiteNav from './site-nav'

class SiteMenu extends React.Component {
  constructor() {
    super();

    this.state = {
      menuIsActive: false,
      activeClassName: 'menu-isActive'
    }
  }

  handleMenuClick() {
    const _className = this.state.activeClassName;
    const _menuIsActive = this.state.menuIsActive;

    if (_menuIsActive) {
      document.body.classList.remove(_className);
    } else {
      document.body.classList.add(_className);
    }

    this.setState({
      menuIsActive: !_menuIsActive
    })
  }

  handleNavClick() {
    if (this.state.menuIsActive) {
      document.body.classList.remove(this.state.activeClassName);

      this.setState({
        menuIsActive: false
      })
    }
  }

  render() {
    return (
      <div className="site-menu">
        <SiteNav className="nav-slide-right" onClick={ this.handleNavClick.bind(this) } />
        <a className="site-menu__menu" onClick={ this.handleMenuClick.bind(this) }>
          <div className="site-menu__hamburger"><span></span></div>
        </a>
      </div>
    );
  }
}

export default SiteMenu;
