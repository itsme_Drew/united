import React from 'react'
import IconPhone from '../../icons/phone'
import IconPin from '../../icons/pin'

class NavUtility extends React.Component {
  render() {
    return (
      <nav className="utility">
          <a className="utility__link" href="https://www.google.com/maps/dir//27.251126,-82.498036/@27.251126,-82.498036,16z?hl=en-US" target="_blank" rel="noopener noreferrer">
            <IconPin />
            <span>Directions</span>
          </a>
          <a className="utility__link phone" href="tel:+19419224222">
            <IconPhone />
            <span>(941) 922-4222</span>
          </a>

      </nav>
    )
  }
}

export default NavUtility;
