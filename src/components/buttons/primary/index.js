import React from 'react'
import { Link } from 'react-router-dom'
import classnames from 'classnames'
import PropTypes from 'prop-types'

class ButtonPrimary extends React.Component {
  renderClick(e) {
    e.preventDefault();

    this.props.onClick();
  }

  render() {
    return(
      <Link
          className={classnames('btn-primary', this.props.className)}
          to={this.props.linkTo}
          target={this.props.linkTarget}
          >{this.props.children}</Link>
    )
  }
}

ButtonPrimary.propTypes = {
  linkTo: PropTypes.string.isRequired,
  linkTarget: PropTypes.string
}

export default ButtonPrimary;
