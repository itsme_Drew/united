import React from 'react'

class Star extends React.Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33 33">
        <title>Star</title>
        <polygon className="star" points="21.643 11.092 16.5 0.671 11.357 11.092 -0.143 12.763 8.178 20.875 6.214 32.329 16.5 26.921 26.786 32.329 24.822 20.875 33.143 12.763 21.643 11.092"/>
      </svg>
    )
  }
}

export default Star;
