import React from 'react'
import Star from './star'
import PropTypes from 'prop-types'

class Stars extends React.Component {
  constructor() {
    super();

    this.renderStars = this.renderStars.bind(this);
  }

  renderStars() {
    const starEl = [];

    for (var i = 0; i < this.props.count; i++) {
      starEl.push(<div className="stars__star" key={i}><Star key={i} /></div>)
    }

    return starEl;
  }

  render() {
    return (
      <div className="stars">
        {this.renderStars()}
      </div>
    )
  }
}

Stars.propTypes = {
  count: PropTypes.string.isRequired
}

export default Stars;
