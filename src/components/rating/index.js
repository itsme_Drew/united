import React from 'react'
import Stars from './stars'
import PropTypes from 'prop-types'

class Rating extends React.Component {
  render() {
    return (
      <div className="rating">
        <Stars count={this.props.count} />
        <a href="https://www.google.com/search?q=united+joint+and+spine+center&oq=united+joint+and+&aqs=chrome.0.0j69i60j69i57j69i59j69i60l2.2831j0j1&sourceid=chrome&ie=UTF-8"
          className="rating__link" target="_blank" rel="noopener noreferrer">View Google Reviews</a>
      </div>
    );
  }
}

Rating.propTypes = {
  count: PropTypes.string.isRequired
}

export default Rating;
