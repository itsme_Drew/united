import React from 'react'
import ImageRound from '../../../../components/image/round'
import PropTypes from 'prop-types'

class PainCategory extends React.Component {
  render() {
    return (
      <div className="pain-category col--md-3 col--sm-6">
        <ImageRound className="pain-category__image" imgSrc={this.props.imgSrc} alt={this.props.label} />
        <span className="pain-category__label">{this.props.label}</span>
      </div>
    )
  }
}

PainCategory.propTypes = {
  imgSrc: PropTypes.string,
  label: PropTypes.string
}

export default PainCategory;
