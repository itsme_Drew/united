import React from 'react'
import PainCategory from './category'

class PainCategories extends React.Component {

  render() {
    const CatList = ["headaches", "backpain", "neck pain", "scoliosis", "shoulder & arm", "stress", "hip"]
    let categories = []

    for (var i = 0; i < CatList.length; i++) {
      categories.push(<PainCategory label={CatList[i]} key={i} />);
    }

    return (
      <div className="pain-categories row">
        {categories}
      </div>
    )

  }
}

export default PainCategories;
