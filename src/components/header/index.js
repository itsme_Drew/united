import React from 'react'
import {Link} from 'react-router-dom'
import SiteMenu from '../nav/site-menu/index.js'
import NavUtility from '../nav/utility/index.js'
import LogoUnited from '../logos/united/index.js'

class Header extends React.Component {
  render() {
    return (
      <header className="site-header">
        <div className="site-header__logo">
          <Link to="/"><LogoUnited /></Link>
        </div>
        <div className="site-header__nav">
          <NavUtility />
          <SiteMenu />
        </div>
      </header>
    );
  }
}

export default Header;
