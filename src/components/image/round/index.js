import React from 'react'
import PropTypes from 'prop-types'

class ImageRound extends React.Component {
  render() {
    return (
      <div className="img-round__container">
        <img className="img-round__img" src={this.props.imgSrc || 'http://www.lorempixel.com/1000/1000'} alt={this.props.alt || 'united joint health care'}/>
      </div>
    );
  }
}

ImageRound.propTypes = {
  alt: PropTypes.string,
  imgSrc: PropTypes.string,
  imgCaption: PropTypes.string
}

export default ImageRound;
