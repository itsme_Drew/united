import React from 'react'
import PropTypes from 'prop-types'

class ImageCaption extends React.Component {
  constructor() {
    super();

    this.renderCaption = this.renderCaption.bind(this);
  }

  renderCaption() {
    return <span className="img__caption">{this.props.imgCaption}</span>
  }

  render() {
    return (
      <div className="img">
        <img className="img__img" src={this.props.imgSrc || 'http://www.lorempixel.com/1000/1000'} alt={this.props.alt || 'feature'}/>
        {this.props.imgCaption ? this.renderCaption() : null}
      </div>
    );
  }
}

ImageCaption.propTypes = {
  alt: PropTypes.string,
  imgSrc: PropTypes.string,
  imgCaption: PropTypes.string
}

export default ImageCaption;
