import React from 'react'
import StaffListing from '../listing'
import PortraitDemaio from '../../../images/portrait-demaio.jpg'

class StaffDemaio extends React.Component {
  render() {
    return (
      <StaffListing name="Dr. DeMaio" position="Chiropractor" imgSrc={PortraitDemaio}>
        <p>I began my Chiropractic journey at Logan Collage of Chiropractic in St Louis Missouri and graduated in 2010. I started practicing in Sarasota in 2011 with a mission to help as many patients achieve optimal wellness and enjoy a naturally pain free life through Chiropractic care.</p>
        <p>United Joint & Spine Center is a great place for patients of all ages to either get well, be well, and or stay well. I pride myself on excellent patient centered care and practice a wide range of techniques to get the best results for each patient.</p>
      </StaffListing>
    )
  }
}

export default StaffDemaio;
