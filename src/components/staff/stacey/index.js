import React from 'react'
import StaffListing from '../listing'
import PortraitStacey from '../../../images/portrait-stacey.jpg'

class Staffstacey extends React.Component {
  render() {
    return (
      <StaffListing name="stacey" position="office manager" imgSrc={PortraitStacey}>
        <p>I was born and raised in Sarasota, Fl. I knew I wanted to be part of United Joint & Spine Centers team because of the strong passion that the office has about saving lives through chiropractic care. Being responsible for the daily operations of United Joint & Spine Center, I have learned through my own personal experience with pain what true wellness is and understand the benefits of regular chiropractic care.</p>
        <p>There are so many things I would love to share about our practice that separates us from others, however I feel the most important is when you become a patient, you truly will feel welcomed, educated and a overall sense of wanting to start a pure natural healthier lifestyle. I am honored each day to come to work at a place full of health and happiness. At United Joint & Spine Center, we will do everything in our power to help you live the best life you can live. Chiropractic saved my life, now its time for chiropractic to save yours!! See you at our office!!</p>
      </StaffListing>
    )
  }
}

export default Staffstacey;
