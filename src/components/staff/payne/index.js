import React from 'react'
import StaffListing from '../listing'
import PortraitPayne from '../../../images/portrait-payne.jpg'

class Staffstacey extends React.Component {
  render() {
    return (
      <StaffListing name="Dr. Luciana Payne" position="chiropractor" imgSrc={PortraitPayne}>
        <p>I was born and raised in Buffalo, NY. I attended D'Youville College in Buffalo, NY for my undergraduate and Chiropractic degrees, where I was also a member of the women's volleyball and tennis teams. I saw the benefits of Chiropractic care at an early age while being involved in different sports growing up.</p> 
        <p>Many patients call me "Dr. Out of Payne," as I am passionate about treating people of all ages and showing them the positive effects of Chiropractic care. I pride myself on patient education and wellness. I am happy to be a part of the fantastic team at United Joint & Spine Center.</p>
      </StaffListing>
    )
  }
}

export default Staffstacey;
