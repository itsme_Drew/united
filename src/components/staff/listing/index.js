import React from 'react'
import PropTypes from 'prop-types'
import ImageRound from '../../image/round'

class StaffListing extends React.Component {
  render() {
    return (
      <div className="staff-listing row">
        <div className="staff-listing__portrait col col--md-4">
          <ImageRound imgSrc={this.props.imgSrc} alt={this.props.name} />
        </div>
        <div className="staff-listing__details col col--md-8">
          <div className="staff-listing__header">
            <h3 className="staff-listing__name">{this.props.name}</h3>
            <span className="staff-listing__position">{this.props.position}</span>
          </div>
          {this.props.children}
        </div>
      </div>
    )
  }
}

StaffListing.propTypes = {
  imgSrc: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired
}

export default StaffListing;
