import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import IconHeartSearch from '../../components/icons/heart-search'
import IconHeartPadlock from '../../components/icons/heart-padlock'
import IconHeartCheck from '../../components/icons/heart-check'

class Plan extends React.Component {
  render() {
    return (
      <Tabs className="plan-tabs">
        <TabList className="plan-tabs__nav">
          <Tab className="plan-tabs__nav-item"><IconHeartSearch className="plan-tabs__nav-icon" />get well</Tab>
          <Tab className="plan-tabs__nav-item"><IconHeartCheck className="plan-tabs__nav-icon" />be well</Tab>
          <Tab className="plan-tabs__nav-item"><IconHeartPadlock className="plan-tabs__nav-icon" />stay well</Tab>
        </TabList>

        <TabPanel className="plan-tabs__content">
          <p>Relief care focuses on <b>JUST</b> the symptoms. In this case it is the headache. Relief care would be similar to taking aspirin. Although it temporarily alleviate the headaches, it does nothing to correct the cause.</p>
        </TabPanel>
        <TabPanel className="plan-tabs__content">
          <p>The goal of corrective care is the removal or reduction of the cause of your problem, allowing the relief or removal of the symptoms. Corrective care is necessary not only to relieve or reduce a patient’s pain or symptoms, but to also remove the actual cause of the problem.</p>
        </TabPanel>
        <TabPanel className="plan-tabs__content">
          <p>Taking correct care of our bodies is very important in our everyday lives. Without continued chiropractic care, your muscles and vertebrae will return to their unhealthy positions. Part of the stay well step retrains and strengthens muscles to assume a more correct position. This is optimal care at it's finest.</p>
        </TabPanel>
      </Tabs>
    )
  }
}

export default Plan;
