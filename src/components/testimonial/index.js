import React from 'react'

const testimonialList = [
  {author: "Connie (Google Reviewer)", "comment": "Dr. DeMaio is the one and ONLY chiropractor that I will go to. She takes the time to explain why I have the pain I am experiencing and how she will treat my pain. I trust her and her staff."},
  {author: "Kerri (Google Reviewer)", "comment": "Dr. DeMaio and Stacey have been nothing short but amazing. I had been in a car accident a year ago and I was so afraid to go to a chiropractor, but Dr. DeMaio made the experience great. They were able to get me in that very same day, and always worked with my schedule."},
  {author: "Russ (Google Reviewer)", "comment": "Dr. DeMaio and Stacey are really great! The atmosphere is clean and it has up to date modern technology. The treatments really helped me out with my back pain now I feel AWESOME!"},
  {author: "N S (Google Reviewer)", "comment": "Hands down the best chiropractic experience I've ever had. They truly care and take the time to ensure that your needs are met and concerns are addressed. A genuine family atmosphere where you will be treated with the utmost professionalism."},
  {author: "Ray (Google Reviewer)", "comment": "Dr DeMaio is the BEST chiropractor that I have EVER been to. She always takes the time to sit and listen to what pain I am experiencing. I had a recent fall that caused my whole knee bruise. Dr. DeMaio suggested laser along with treatment, and it worked GREAT!!"},
]

class Testimonial extends React.Component {
  constructor() {
    super();

    this.state = {
      testimonial: {
        author: '',
        comment: ''
      }
    }

    this.renderTestimonial = this.renderTestimonial.bind(this);
    this.generateNumber = this.generateNumber.bind(this);
  }

  componentWillMount() {
    this.renderTestimonial();
  }

  renderTestimonial() {
    const selectedTestimonial = this.generateNumber(0, testimonialList.length);

    this.setState({
      testimonial: testimonialList[selectedTestimonial]
    })
  }

  generateNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min)) + min;
  }

  render() {
    return (
      <div className="testimonial">
        <span className="testimonial__message">
          {this.state.testimonial.comment}
        </span>
        <span className="testimonial__author">{this.state.testimonial.author}</span>
      </div>
    )
  }
}

export default Testimonial;
